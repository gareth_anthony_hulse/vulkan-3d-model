/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

vk::ImageView
app::vulkan::m_create_image_view (vk::Image image, vk::Format format, vk::ImageAspectFlags aspect_flags, uint32_t mip_levels)
{
  vk::ImageViewCreateInfo view_info = vk::ImageViewCreateInfo ()
    .setImage (image)
    .setViewType (vk::ImageViewType::e2D)
    .setFormat (format)
    .setSubresourceRange (vk::ImageSubresourceRange ()
			  .setAspectMask (aspect_flags)
			  .setBaseMipLevel (0)
			  .setLevelCount (mip_levels)
			  .setBaseArrayLayer (0)
			  .setLayerCount (1));

  vk::ImageView image_view;
  if (m_logical_device.createImageView (&view_info, nullptr, &image_view) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create texture image view.");
    }

  return image_view;
}
