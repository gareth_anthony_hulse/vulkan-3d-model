/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

uint32_t
app::vulkan::m_find_memory_type (uint32_t type_filter, vk::MemoryPropertyFlags properties)
{
  vk::PhysicalDeviceMemoryProperties mem_properties;
  m_physical_device.getMemoryProperties (&mem_properties);

  for (uint32_t i = 0; i < mem_properties.memoryTypeCount; ++i)
    {
      if ((type_filter & (1 << i)) &&
	  (mem_properties.memoryTypes[i].propertyFlags & properties) == properties)
	{
	  return i;
	}
    }
  throw std::runtime_error ("Failed to find suitable memory type.");
}
