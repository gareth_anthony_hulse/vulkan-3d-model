/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_init_window ()
{
  glfwInit ();
  GLFWmonitor *monitor = glfwGetPrimaryMonitor ();
  const GLFWvidmode *mode = glfwGetVideoMode (monitor);
  m_refresh_rate = std::chrono::duration<double> (1.0 / mode->refreshRate);
  
  // Set cursor to center of the screen.
  m_last_x = mode->width / 2;
  m_last_y = mode->height / 2;

  glfwWindowHint (GLFW_CLIENT_API, GLFW_NO_API);
  
  m_window = glfwCreateWindow (mode->width, mode->height, PACKAGE_NAME, monitor, nullptr);
  glfwMakeContextCurrent(m_window);

  // Mouse:
  glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSetWindowUserPointer (m_window, this);
  glfwSetInputMode (m_window, GLFW_STICKY_KEYS, GLFW_TRUE);
  
  glfwSetFramebufferSizeCallback (m_window, m_framebuffer_resize_callback);
}
