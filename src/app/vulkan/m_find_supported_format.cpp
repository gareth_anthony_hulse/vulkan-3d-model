/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

vk::Format
app::vulkan::m_find_supported_format (const std::vector<vk::Format>& candidates, vk::ImageTiling tiling,
				       vk::FormatFeatureFlags features)
{
  for (vk::Format format : candidates)
    {
      vk::FormatProperties props;
      m_physical_device.getFormatProperties (format, &props);

      if (tiling == vk::ImageTiling::eLinear && (props.linearTilingFeatures & features) == features)
	{
	  return format;
	}
      else if (tiling == vk::ImageTiling::eOptimal && (props.optimalTilingFeatures & features) == features)
	{
	  return format;
	}
    }
  throw std::runtime_error ("Failed to find supported format.");
}
