

#include "../vulkan.hpp"

void
app::vulkan::m_end_single_time_commands (vk::CommandBuffer command_buffer)
{
  command_buffer.end ();

  vk::SubmitInfo submit_info = vk::SubmitInfo ()
    .setCommandBufferCount (1)
    .setPCommandBuffers (&command_buffer);

  m_graphics_queue.submit (1, &submit_info, nullptr);
  m_graphics_queue.waitIdle ();

  m_logical_device.freeCommandBuffers (m_command_pool, 1, &command_buffer);
}
