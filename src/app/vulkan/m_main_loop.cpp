/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"
#include <iostream>

void
app::vulkan::m_main_loop ()
{
  double last_time = glfwGetTime ();
  double current_time = 0;
  
  while (!glfwWindowShouldClose (m_window))
    {
      current_time = glfwGetTime ();
      m_delta_time = current_time - last_time;
      last_time = current_time;
      
      m_get_cursor_input (m_window);
      m_get_key_input (m_window);

      m_draw_frame ();
      glfwPollEvents ();
    }

  m_logical_device.waitIdle ();
}
