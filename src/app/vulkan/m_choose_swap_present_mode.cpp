/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

vk::PresentModeKHR
app::vulkan::m_choose_swap_present_mode (const std::vector<vk::PresentModeKHR>& available_present_modes)
{
  for (const auto& available_present_mode : available_present_modes)
    {
      // With Wayland, there should be no screen tearing.
      if (available_present_mode == vk::PresentModeKHR::eImmediate &&
	  getenv ("XDG_SESSION_TYPE") == m_target_session_type)
	{
	  return available_present_mode;
	}
      if (available_present_mode == vk::PresentModeKHR::eMailbox)
	{
	  return available_present_mode;
	}
    }
  return vk::PresentModeKHR::eFifo;
}
