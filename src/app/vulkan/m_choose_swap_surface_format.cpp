/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

vk::SurfaceFormatKHR
app::vulkan::m_choose_swap_surface_format (const std::vector<vk::SurfaceFormatKHR>& available_formats)
{
  for (const auto& available_format : available_formats)
    {
      if (available_format.format == vk::Format::eB8G8R8A8Unorm &&
	  available_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
	{
	  return available_format;
	}
    }
  return available_formats[0];
}
