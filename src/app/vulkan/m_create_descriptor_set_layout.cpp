/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_descriptor_set_layout ()
{
  vk::DescriptorSetLayoutBinding ubo_layout_binding = vk::DescriptorSetLayoutBinding ()
    .setBinding (0)
    .setDescriptorCount (1)
    .setDescriptorType (vk::DescriptorType::eUniformBuffer)
    .setPImmutableSamplers (nullptr)
    .setStageFlags (vk::ShaderStageFlagBits::eVertex);

  vk::DescriptorSetLayoutBinding sampler_layout_binding = vk::DescriptorSetLayoutBinding ()
    .setBinding (1)
    .setDescriptorCount (1)
    .setDescriptorType (vk::DescriptorType::eCombinedImageSampler)
    .setPImmutableSamplers (nullptr)
    .setStageFlags (vk::ShaderStageFlagBits::eFragment);

  std::array<vk::DescriptorSetLayoutBinding, 2> bindings = {ubo_layout_binding, sampler_layout_binding};

  vk::DescriptorSetLayoutCreateInfo layout_info = vk::DescriptorSetLayoutCreateInfo ()
    .setBindingCount (static_cast<uint32_t> (bindings.size ()))
    .setPBindings (bindings.data ());

  if (m_logical_device.createDescriptorSetLayout (&layout_info, nullptr, &m_descriptor_set_layout) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create descriptor set layout.");
    }
}
