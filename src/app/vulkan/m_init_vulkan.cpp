/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_init_vulkan ()
{
  m_create_instance (); // Done.
#ifndef NDEBUG
  m_setup_debug_messenger ();
#endif //NDEBUG
  m_create_surface ();
  m_pick_physical_device (); // Done.
  m_create_logical_device (); // Done.
  m_create_swap_chain ();
  m_create_image_views ();
  m_create_render_pass ();
  m_create_descriptor_set_layout ();
  m_create_graphics_pipeline ();
  m_create_color_resources ();
  m_create_depth_resources ();
  m_create_framebuffers ();
  m_create_command_pool ();
  m_create_texture_image ();
  m_create_texture_image_view ();
  m_create_texture_sampler ();
  m_load_model ();
  m_create_vertex_buffer ();
  m_create_index_buffer ();
  m_create_uniform_buffers ();
  m_create_descriptor_pool ();
  m_create_descriptor_sets ();
  m_create_command_buffers ();
  m_create_sync_objects ();
}
