/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_uniform_buffers ()
{
  vk::DeviceSize buffer_size = sizeof (app::vulkan::uniform_buffer_object);

  m_uniform_buffers.resize (m_swap_chain_images.size ());
  m_uniform_buffers_memory.resize (m_swap_chain_images.size ());

  for (size_t i = 0; i < m_swap_chain_images.size (); ++i)
    {
      m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, m_uniform_buffers[i], m_uniform_buffers_memory[i]);
    }
}
