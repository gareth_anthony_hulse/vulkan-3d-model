/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_texture_sampler ()
{
  vk::SamplerCreateInfo sampler_info = vk::SamplerCreateInfo ()
    .setMagFilter (vk::Filter::eLinear)
    .setMinFilter (vk::Filter::eLinear)
    .setAddressModeU (vk::SamplerAddressMode::eRepeat)
    .setAddressModeV (vk::SamplerAddressMode::eRepeat)
    .setAddressModeW (vk::SamplerAddressMode::eRepeat)
    .setAnisotropyEnable (VK_TRUE)
    .setMaxAnisotropy (16)
    .setBorderColor (vk::BorderColor::eIntOpaqueBlack)
    .setUnnormalizedCoordinates (VK_FALSE)
    .setCompareEnable (VK_FALSE)
    .setCompareOp (vk::CompareOp::eAlways)
    .setMipmapMode (vk::SamplerMipmapMode::eLinear)
    .setMipLodBias (0.0f)
    .setMinLod (0.0f)
    .setMaxLod (static_cast<float> (m_mip_levels));

  if (m_logical_device.createSampler (&sampler_info, nullptr, &m_texture_sampler) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create texture sampler.");
    }
}
