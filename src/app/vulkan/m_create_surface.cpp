/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_surface ()
{
  if (glfwCreateWindowSurface (m_instance, m_window, nullptr, &m_surface) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to create window surface!");
    }
}
