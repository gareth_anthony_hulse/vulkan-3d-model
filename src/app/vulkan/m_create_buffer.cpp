/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_buffer (vk::DeviceSize size,
			      vk::BufferUsageFlags usage,
			      vk::MemoryPropertyFlags properties,
			      vk::Buffer& buffer,
			      vk::DeviceMemory& buffer_memory)
{
  vk::BufferCreateInfo buffer_info = vk::BufferCreateInfo ()
    .setSize (size)
    .setUsage (usage)
    .setSharingMode (vk::SharingMode::eExclusive);

  if (m_logical_device.createBuffer (&buffer_info, nullptr, &buffer) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create buffer!");
    }

  vk::MemoryRequirements mem_requirements;
  m_logical_device.getBufferMemoryRequirements (buffer, &mem_requirements);

  vk::MemoryAllocateInfo alloc_info = vk::MemoryAllocateInfo ()
    .setAllocationSize (mem_requirements.size)
    .setMemoryTypeIndex (m_find_memory_type (mem_requirements.memoryTypeBits, properties));

  if (m_logical_device.allocateMemory (&alloc_info, nullptr, &buffer_memory) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to allocate buffer memory!");
    }

  m_logical_device.bindBufferMemory (buffer, buffer_memory, 0);
}
