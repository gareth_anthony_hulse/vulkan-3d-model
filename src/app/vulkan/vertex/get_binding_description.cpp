/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../../vulkan.hpp"

vk::VertexInputBindingDescription
app::vulkan::vertex::get_binding_description ()
{
  vk::VertexInputBindingDescription binding_description = vk::VertexInputBindingDescription ()
    .setBinding (0)
    .setStride (sizeof (vertex))
    .setInputRate (vk::VertexInputRate::eVertex);

  return binding_description;
}
