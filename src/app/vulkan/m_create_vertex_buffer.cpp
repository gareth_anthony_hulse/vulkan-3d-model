/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_vertex_buffer ()
{
  vk::DeviceSize buffer_size = sizeof (m_vertices[0]) * m_vertices.size ();

  vk::Buffer staging_buffer;
  vk::DeviceMemory staging_buffer_memory;
  m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, staging_buffer, staging_buffer_memory);

  void* data;
  vkMapMemory (m_logical_device, staging_buffer_memory, 0, buffer_size, 0, &data);
  memcpy (data, m_vertices.data (), static_cast<size_t> (buffer_size));
  m_logical_device.unmapMemory (staging_buffer_memory);

  m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal, m_vertex_buffer, m_vertex_buffer_memory);

  m_copy_buffer (staging_buffer, m_vertex_buffer, buffer_size);

  m_logical_device.destroyBuffer (staging_buffer, nullptr);
  m_logical_device.freeMemory (staging_buffer_memory, nullptr);
}
