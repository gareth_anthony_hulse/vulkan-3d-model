/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

std::vector<vk::ExtensionProperties>
app::vulkan::m_get_available_device_extensions (const vk::PhysicalDevice& physical_device) const
{
  uint32_t extension_count;
  physical_device.enumerateDeviceExtensionProperties (nullptr, &extension_count, nullptr);
  
  std::vector<vk::ExtensionProperties> extensions (extension_count);
  physical_device.enumerateDeviceExtensionProperties (nullptr, &extension_count, extensions.data ());

  return extensions;
}
