/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_pick_physical_device ()
{
  m_physical_device = m_instance.enumeratePhysicalDevices ().front ();

  if (!m_physical_device)
    {
      throw std::runtime_error ("Failed to find suitable physical device.");
    }
  m_msaa_samples = m_get_max_usable_smaple_count ();
}
