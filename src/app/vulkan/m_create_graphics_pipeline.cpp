/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_graphics_pipeline ()
{
  auto vert_shader_code = m_read_file (PKGDATADIR "/shaders/triangle-vert.spv");
  auto frag_shader_code = m_read_file (PKGDATADIR "/shaders/triangle-frag.spv");

  vk::ShaderModule vert_shader_module = m_create_shader_module (vert_shader_code);
  vk::ShaderModule frag_shader_module = m_create_shader_module (frag_shader_code);

  vk::PipelineShaderStageCreateInfo vert_shader_stage_info = vk::PipelineShaderStageCreateInfo ()
    .setStage (vk::ShaderStageFlagBits::eVertex)
    .setModule (vert_shader_module)
    .setPName ("main");

  vk::PipelineShaderStageCreateInfo frag_shader_stage_info = vk::PipelineShaderStageCreateInfo ()
    .setStage (vk::ShaderStageFlagBits::eFragment)
    .setModule (frag_shader_module)
    .setPName ("main");

  vk::PipelineShaderStageCreateInfo shader_stages[] =
    {
     vert_shader_stage_info,
     frag_shader_stage_info
    };

  auto binding_description = app::vulkan::vertex::get_binding_description ();
  auto attribute_descriptions = app::vulkan::vertex::get_attribute_descriptions ();
  
  vk::PipelineVertexInputStateCreateInfo vertex_input_info = vk::PipelineVertexInputStateCreateInfo ()
    .setVertexBindingDescriptionCount (1)
    .setVertexAttributeDescriptionCount (static_cast<uint32_t> (attribute_descriptions.size ()))
    .setPVertexBindingDescriptions (&binding_description)
    .setPVertexAttributeDescriptions (attribute_descriptions.data ());

  vk::PipelineInputAssemblyStateCreateInfo input_assembly = vk::PipelineInputAssemblyStateCreateInfo ()
    .setTopology (vk::PrimitiveTopology::eTriangleList)
    .setPrimitiveRestartEnable (VK_FALSE);

  vk::Viewport viewport = vk::Viewport ()
    .setX (0.0f)
    .setY (0.0f)
    .setWidth (static_cast<float> (m_swap_chain_extent.width))
    .setHeight (static_cast<float> (m_swap_chain_extent.height))
    .setMinDepth (0.0f)
    .setMaxDepth (1.0f);

  vk::Rect2D scissor = vk::Rect2D ()
    .setOffset({0, 0})
    .setExtent (m_swap_chain_extent);

  vk::PipelineViewportStateCreateInfo viewport_state = vk::PipelineViewportStateCreateInfo ()
    .setViewportCount (1)
    .setPViewports (&viewport)
    .setScissorCount (1)
    .setPScissors (&scissor);

  vk::PipelineRasterizationStateCreateInfo rasterizer = vk::PipelineRasterizationStateCreateInfo ()
    .setDepthClampEnable (VK_FALSE)
    .setRasterizerDiscardEnable (VK_FALSE)
    .setPolygonMode (vk::PolygonMode::eFill)
    .setLineWidth (1.0f)
    .setCullMode (vk::CullModeFlagBits::eBack)
    .setFrontFace (vk::FrontFace::eCounterClockwise)
    .setDepthBiasEnable (VK_FALSE);

  vk::PipelineMultisampleStateCreateInfo multisampling = vk::PipelineMultisampleStateCreateInfo ()
    .setSampleShadingEnable (VK_TRUE)
    .setRasterizationSamples (m_msaa_samples)
    .setMinSampleShading (1.0f);

  vk::PipelineDepthStencilStateCreateInfo depth_stencil = vk::PipelineDepthStencilStateCreateInfo ()
    .setDepthTestEnable (VK_TRUE)
    .setDepthWriteEnable (VK_TRUE)
    .setDepthCompareOp (vk::CompareOp::eLess)
    .setDepthBoundsTestEnable ( VK_FALSE);

  vk::PipelineColorBlendAttachmentState color_blend_attachment = vk::PipelineColorBlendAttachmentState ()
    .setColorWriteMask
    (vk::ColorComponentFlagBits::eR |
     vk::ColorComponentFlagBits::eG |
     vk::ColorComponentFlagBits::eB |
     vk::ColorComponentFlagBits::eA)
    .setBlendEnable (VK_FALSE);

  vk::PipelineColorBlendStateCreateInfo color_blending = vk::PipelineColorBlendStateCreateInfo ()
    .setLogicOpEnable (VK_FALSE)
    .setAttachmentCount(1)
    .setPAttachments (&color_blend_attachment);

  vk::PipelineLayoutCreateInfo pipeline_layout_info = vk::PipelineLayoutCreateInfo ()
    .setSetLayoutCount (1)
    .setPSetLayouts (&m_descriptor_set_layout);

  if (m_logical_device.createPipelineLayout (&pipeline_layout_info, nullptr, &m_pipeline_layout)
      != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create pipeline layout.");
    }

  vk::GraphicsPipelineCreateInfo pipeline_info = vk::GraphicsPipelineCreateInfo ()
    .setStageCount (2)
    .setPStages (shader_stages)
    .setPVertexInputState  (&vertex_input_info)
    .setPInputAssemblyState (&input_assembly)
    .setPViewportState (&viewport_state)
    .setPRasterizationState (&rasterizer)
    .setPMultisampleState (&multisampling)
    .setPDepthStencilState (&depth_stencil)
    .setPColorBlendState (&color_blending)
    .setLayout (m_pipeline_layout)
    .setRenderPass (m_render_pass)
    .setSubpass (0);

  if (m_logical_device.createGraphicsPipelines (nullptr, 1, &pipeline_info, nullptr, &m_graphics_pipeline) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create graphics pipeline.");
    }
  
  m_logical_device.destroyShaderModule (frag_shader_module, nullptr);
  m_logical_device.destroyShaderModule (vert_shader_module, nullptr);
}
