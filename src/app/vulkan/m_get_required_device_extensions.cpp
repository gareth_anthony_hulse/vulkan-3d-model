/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

std::vector<const char*>
app::vulkan::m_get_required_device_extensions () const
{
  return {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
}
