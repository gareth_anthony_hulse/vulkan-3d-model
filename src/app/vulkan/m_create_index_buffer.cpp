/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_index_buffer ()
{
  vk::DeviceSize buffer_size = sizeof (m_indicies[0]) * m_indicies.size ();

  vk::Buffer staging_buffer;
  vk::DeviceMemory staging_buffer_memory;
  m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, staging_buffer, staging_buffer_memory);

  void* data;
  vkMapMemory (m_logical_device, staging_buffer_memory, 0, buffer_size, 0, &data);
  memcpy (data, m_indicies.data (), static_cast<size_t> (buffer_size));
  m_logical_device.unmapMemory (staging_buffer_memory);

  m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal, m_index_buffer, m_index_buffer_memory);

  m_copy_buffer (staging_buffer, m_index_buffer, buffer_size);

  m_logical_device.destroyBuffer (staging_buffer, nullptr);
  m_logical_device.freeMemory (staging_buffer_memory, nullptr);
}
