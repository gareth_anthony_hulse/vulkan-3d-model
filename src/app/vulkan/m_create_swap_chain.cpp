/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_swap_chain ()
{
  swap_chain_support_details swap_chain_support = m_query_swap_chain_support (m_physical_device);
  vk::SurfaceFormatKHR surface_format = m_choose_swap_surface_format (swap_chain_support.formats);
  vk::PresentModeKHR present_mode = m_choose_swap_present_mode (swap_chain_support.present_modes);
  vk::Extent2D extent = m_choose_swap_extent (swap_chain_support.capabilities);

  uint32_t image_count = swap_chain_support.capabilities.minImageCount + 1;
  if (swap_chain_support.capabilities.maxImageCount > 0 && image_count > swap_chain_support.capabilities.maxImageCount)
    {
      image_count = swap_chain_support.capabilities.maxImageCount;
    }

  vk::SwapchainCreateInfoKHR create_info = vk::SwapchainCreateInfoKHR ()
    .setSurface (m_surface)
    .setMinImageCount (image_count)
    .setImageFormat (surface_format.format)
    .setImageColorSpace (surface_format.colorSpace)
    .setImageExtent (extent)
    .setImageArrayLayers (1)
    .setImageUsage (vk::ImageUsageFlagBits::eColorAttachment)
    .setPreTransform (swap_chain_support.capabilities.currentTransform)
    .setCompositeAlpha (vk::CompositeAlphaFlagBitsKHR::eOpaque)
    .setPresentMode (present_mode)
    .setClipped (VK_TRUE)
    .setOldSwapchain (m_swap_chain);

  queue_family_indices indices = m_find_queue_families (m_physical_device);
  uint32_t my_queue_family_indices[] =
    {
     indices.graphics_family.value (),
     indices.present_family.value ()
    };

  if (indices.graphics_family != indices.present_family)
    {
      create_info
	.setImageSharingMode (vk::SharingMode::eConcurrent)
	.setQueueFamilyIndexCount (2) 
	.setPQueueFamilyIndices (my_queue_family_indices);
    }
  else
    {
      create_info.setImageSharingMode (vk::SharingMode::eExclusive);
    }

  if (m_logical_device.createSwapchainKHR (&create_info, nullptr, &m_swap_chain) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create swap chain!");
    }

  vkGetSwapchainImagesKHR (m_logical_device, m_swap_chain, &image_count, nullptr);
  m_swap_chain_images.resize (image_count);
  m_logical_device.getSwapchainImagesKHR (m_swap_chain, &image_count, m_swap_chain_images.data ());

  m_swap_chain_image_format = surface_format.format;
  m_swap_chain_extent = extent;
}
