/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef NDEBUG
#include "../vulkan.hpp"

void
app::vulkan::m_setup_debug_messenger ()
{
  VkDebugUtilsMessengerCreateInfoEXT create_info;
  m_populate_debug_messenger_create_info (create_info);
  
  if (m_create_debug_messenger (m_instance, &create_info, nullptr, &m_debug_messenger) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to setup debug messenger!");
    }
}
#endif //NDEBUG
