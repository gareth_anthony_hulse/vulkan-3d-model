/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_command_pool ()
{
  app::vulkan::queue_family_indices my_queue_family_indices = m_find_queue_families (m_physical_device);

  vk::CommandPoolCreateInfo pool_info = vk::CommandPoolCreateInfo ()
    .setQueueFamilyIndex (my_queue_family_indices.graphics_family.value ());

  if (m_logical_device.createCommandPool (&pool_info, nullptr, &m_command_pool) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create command pool.");
    }
}
