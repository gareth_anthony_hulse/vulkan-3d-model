/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_recreate_swap_chain ()
{
  int width = 0, height = 0;
  while (width == 0 || height == 0)
    {
      glfwGetFramebufferSize (m_window, &width, &height);
      glfwWaitEvents ();
    }
  
  m_logical_device.waitIdle ();

  m_cleanup_swap_chain ();

  m_create_swap_chain ();
  m_create_image_views ();
  m_create_render_pass ();
  m_create_graphics_pipeline ();
  m_create_color_resources ();
  m_create_depth_resources ();
  m_create_framebuffers ();
  m_create_uniform_buffers ();
  m_create_descriptor_pool ();
  m_create_descriptor_sets ();
  m_create_command_buffers ();
}
