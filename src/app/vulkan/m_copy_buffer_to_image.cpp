
#include "../vulkan.hpp"

void
app::vulkan::m_copy_buffer_to_image (vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height)
{
  vk::CommandBuffer command_buffer = m_begin_single_time_commands ();

  vk::BufferImageCopy region = vk::BufferImageCopy ();
  region.setBufferOffset (0);
  region.setBufferRowLength (0);
  region.setBufferImageHeight (0);
  region.imageSubresource.setAspectMask (vk::ImageAspectFlagBits::eColor);
  region.imageSubresource.setMipLevel (0);
  region.imageSubresource.setBaseArrayLayer (0);
  region.imageSubresource.setLayerCount (1);
  region.setImageOffset ({0, 0, 0});
  region.setImageExtent ({width, height, 1});;

  command_buffer.copyBufferToImage (buffer, image, vk::ImageLayout::eTransferDstOptimal, 1, &region);
  
  m_end_single_time_commands (command_buffer);
}
