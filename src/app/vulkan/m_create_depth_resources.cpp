/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_depth_resources ()
{
  vk::Format depth_format = m_find_depth_format ();
  m_create_image (m_swap_chain_extent.width,
		  m_swap_chain_extent.height,
		  1,
		  m_msaa_samples,
		  depth_format,
		  vk::ImageTiling::eOptimal,
		  vk::ImageUsageFlagBits::eDepthStencilAttachment,
		  vk::MemoryPropertyFlagBits::eDeviceLocal,
		  m_depth_image,
		  m_depth_image_memory);
  
  m_depth_image_view = m_create_image_view (m_depth_image, depth_format, vk::ImageAspectFlagBits::eDepth, 1);
}
