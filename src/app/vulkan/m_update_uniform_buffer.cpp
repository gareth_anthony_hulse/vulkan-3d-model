/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_update_uniform_buffer (uint32_t current_image)
{
  uniform_buffer_object ubo = {};
  
  ubo.model = glm::scale (glm::mat4 (1.0f),
			  glm::vec3 (1.0f, 1.0f, 1.0f));

  ubo.model = glm::translate(glm::mat4 (1.0f),
			     glm::vec3 (0.0f, 0.0f, 0.0f));
  
  ubo.view = glm::lookAt (m_camera_pos,
			  m_camera_pos + m_camera_front,
			  m_camera_up);
  
  ubo.proj = glm::perspective (glm::radians (70.0f),
			       m_swap_chain_extent.width /
			       static_cast<float> (m_swap_chain_extent.height),
			       0.1f,
			       10.0f);

  // Flip image.
  ubo.proj[1][1] *= -1;

  void* data;
  vkMapMemory (m_logical_device, m_uniform_buffers_memory[current_image], 0, sizeof (ubo), 0, &data);
  memcpy (data, &ubo, sizeof (ubo));
  vkUnmapMemory (m_logical_device, m_uniform_buffers_memory[current_image]);
}
