/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

vk::ShaderModule
app::vulkan::m_create_shader_module (const std::vector<char>& code)
{
  vk::ShaderModuleCreateInfo create_info = vk::ShaderModuleCreateInfo ()
    .setCodeSize (code.size ())
    .setPCode (reinterpret_cast<const uint32_t*> (code.data ()));

  vk::ShaderModule shader_module;
  if (m_logical_device.createShaderModule (&create_info, nullptr, &shader_module) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create shader module.");
    }

  return shader_module;
}
