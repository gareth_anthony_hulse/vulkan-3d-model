/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::run ()
{
  m_init_window ();
  m_init_vulkan ();
  m_main_loop ();
}
