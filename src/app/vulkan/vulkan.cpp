/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

app::vulkan::~vulkan ()
{ 
  m_cleanup_swap_chain ();

  m_logical_device.destroySampler (m_texture_sampler, nullptr);
  m_logical_device.destroyImageView (m_texture_image_view, nullptr);
  m_logical_device.destroyImage (m_texture_image, nullptr);
  m_logical_device.freeMemory (m_texture_image_memory, nullptr);
  m_logical_device.destroyDescriptorSetLayout (m_descriptor_set_layout, nullptr);
  m_logical_device.destroyBuffer (m_index_buffer, nullptr);
  m_logical_device.freeMemory (m_index_buffer_memory, nullptr);
  m_logical_device.destroyBuffer (m_vertex_buffer, nullptr);
  m_logical_device.freeMemory (m_vertex_buffer_memory, nullptr);
  
  for (uint8_t i = 0; i < m_max_frames_in_flight; ++i)
    {
      m_logical_device.destroySemaphore (m_render_finished_semaphores[i], nullptr);
      m_logical_device.destroySemaphore (m_image_available_semaphores[i], nullptr);
      m_logical_device.destroyFence (m_in_flight_fences[i], nullptr);
    }
  
  m_logical_device.destroyCommandPool (m_command_pool, nullptr);
    m_logical_device.destroy ();
#ifndef NDEBUG
  m_destroy_debug_messenger (m_instance, m_debug_messenger, nullptr);
#endif // NDEBUG
  
  m_instance.destroySurfaceKHR (m_surface, nullptr);
  m_instance.destroy ();
  
  glfwDestroyWindow (m_window);
  
  glfwTerminate ();
}

