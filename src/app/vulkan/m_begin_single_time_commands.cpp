/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

vk::CommandBuffer
app::vulkan::m_begin_single_time_commands ()
{
  vk::CommandBufferAllocateInfo alloc_info = vk::CommandBufferAllocateInfo ()
    .setLevel (vk::CommandBufferLevel::ePrimary)
    .setCommandPool (m_command_pool)
    .setCommandBufferCount (1);
  
  vk::CommandBuffer command_buffer;
  m_logical_device.allocateCommandBuffers (&alloc_info, &command_buffer);

  vk::CommandBufferBeginInfo begin_info = vk::CommandBufferBeginInfo ()
    .setFlags (vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

  command_buffer.begin (&begin_info);

  return command_buffer;
}
