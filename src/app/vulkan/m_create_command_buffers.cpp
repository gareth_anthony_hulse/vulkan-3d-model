/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_command_buffers ()
{
  m_command_buffers.resize (m_swap_chain_framebuffers.size ());

  vk::CommandBufferAllocateInfo alloc_info = vk::CommandBufferAllocateInfo ()
    .setCommandPool (m_command_pool)
    .setLevel (vk::CommandBufferLevel::ePrimary)
    .setCommandBufferCount (static_cast<uint32_t> (m_command_buffers.size ()));

  if (m_logical_device.allocateCommandBuffers (&alloc_info, m_command_buffers.data ()) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to allocate command buffers.");
    }

  for (size_t i = 0; i < m_command_buffers.size (); ++i)
    {
      vk::CommandBufferBeginInfo begin_info = vk::CommandBufferBeginInfo ();

      if (m_command_buffers[i].begin (&begin_info) != vk::Result::eSuccess)
	{
	  throw std::runtime_error ("Failed to begin recording command buffer.");
	}

      vk::RenderPassBeginInfo render_pass_info = vk::RenderPassBeginInfo ()
	.setRenderPass (m_render_pass)
	.setFramebuffer (m_swap_chain_framebuffers[i]);
      render_pass_info.renderArea.setOffset ({0, 0});
      render_pass_info.renderArea.setExtent (m_swap_chain_extent);

      
      std::array <vk::ClearValue, 2> clear_values = {};
      clear_values[0].setColor (vk::ClearColorValue ().setFloat32 ({0.0f, 0.0f, 0.0f, 1.0f}));
      clear_values[1].setDepthStencil (vk::ClearDepthStencilValue(1.0f, 0));

      render_pass_info.clearValueCount = static_cast<uint32_t> (clear_values.size ());
      render_pass_info.pClearValues = clear_values.data ();

      m_command_buffers[i].beginRenderPass (&render_pass_info, vk::SubpassContents::eInline);
      m_command_buffers[i].bindPipeline (vk::PipelineBindPoint::eGraphics, m_graphics_pipeline);

      vk::Buffer vertex_buffers[] = {m_vertex_buffer};
      vk::DeviceSize offsets[] = {0};
      m_command_buffers[i].bindVertexBuffers (0, 1, vertex_buffers, offsets);
      m_command_buffers[i].bindIndexBuffer (m_index_buffer, 0, vk::IndexType::eUint32);
      m_command_buffers[i].bindDescriptorSets (vk::PipelineBindPoint::eGraphics, m_pipeline_layout, 0, 1, &m_descriptor_sets[i], 0, nullptr);
      m_command_buffers[i].drawIndexed (static_cast<uint32_t> (m_indicies.size ()), 1, 0, 0, 0);
      m_command_buffers[i].endRenderPass ();
      m_command_buffers[i].end ();
    }
}
