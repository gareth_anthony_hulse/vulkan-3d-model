/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_image_views ()
{
  m_swap_chain_image_views.resize (m_swap_chain_images.size ());

  for (size_t i = 0; i < m_swap_chain_images.size (); ++i)
    {
      m_swap_chain_image_views[i] = m_create_image_view (m_swap_chain_images[i],
							 m_swap_chain_image_format,
							 vk::ImageAspectFlagBits::eColor,
							 1);
    }
}
