/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define STB_IMAGE_IMPLEMENTATION

#include "../vulkan.hpp"

#include <stb/stb_image.h>

void
app::vulkan::m_create_texture_image ()
{
  int tex_width, tex_height, tex_channels;
  
  stbi_uc* pixels = stbi_load (PKGDATADIR "/textures/chalet.jpg", &tex_width, &tex_height,
			       &tex_channels, STBI_rgb_alpha);
  vk::DeviceSize image_size = tex_width * tex_height * 4;
  m_mip_levels = static_cast<uint32_t> (std::floor (std::log2 (std::max (tex_width, tex_height)))) + 1;

  if (!pixels)
    {
      throw std::runtime_error ("Failed to load texture image.");
    }

  vk::Buffer staging_buffer;
  vk::DeviceMemory staging_buffer_memory;
  m_create_buffer (image_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, staging_buffer, staging_buffer_memory);

  void* data;
  vkMapMemory (m_logical_device, staging_buffer_memory, 0, image_size, 0, &data);
  memcpy (data, pixels, static_cast<size_t> (image_size));
  m_logical_device.unmapMemory (staging_buffer_memory);

  stbi_image_free (pixels);

  m_create_image (tex_width, tex_height, m_mip_levels, vk::SampleCountFlagBits::e1, vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst |
		  vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal, m_texture_image, m_texture_image_memory);

  m_transition_image_layout (m_texture_image, vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal, m_mip_levels);
  
  m_copy_buffer_to_image (staging_buffer, m_texture_image, static_cast<uint32_t> (tex_width), static_cast<uint32_t> (tex_height));

  m_logical_device.destroyBuffer (staging_buffer, nullptr);
  m_logical_device.freeMemory (staging_buffer_memory, nullptr);

  m_generate_mipmaps (m_texture_image, vk::Format::eR8G8B8A8Unorm, tex_width, tex_height, m_mip_levels);
}
