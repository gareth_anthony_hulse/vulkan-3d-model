/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_framebuffer_resize_callback (GLFWwindow* window, int /*width*/, int /*height*/)
{
  auto app = reinterpret_cast<app::vulkan*> (glfwGetWindowUserPointer (window));
  app->m_framebuffer_resized = true;
}
