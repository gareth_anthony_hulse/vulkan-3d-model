/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

vk::Extent2D
app::vulkan::m_choose_swap_extent (const vk::SurfaceCapabilitiesKHR& capabilities)
{
  if (capabilities.currentExtent.width != UINT32_MAX)
    {
      return capabilities.currentExtent;
    }

  int new_width, new_height;
  glfwGetFramebufferSize (m_window, &new_width, &new_height);
  vk::Extent2D actual_extent =
    {
     static_cast<uint32_t> (new_width),
     static_cast<uint32_t> (new_height)
    };
  
  actual_extent.width = std::max (capabilities.minImageExtent.width,
				  std::min (capabilities.maxImageExtent.width,
					    actual_extent.width));
  actual_extent.height = std::max (capabilities.minImageExtent.width,
				   std::min (capabilities.maxImageExtent.height,
					     actual_extent.height));
  return actual_extent;
}
