/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_image (uint32_t width, uint32_t height, uint32_t mip_levels, vk::SampleCountFlagBits num_samples, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage,
			     vk::MemoryPropertyFlags properties, vk::Image& image, vk::DeviceMemory& image_memory)
{
  vk::ImageCreateInfo image_info = vk::ImageCreateInfo ()
    .setImageType (vk::ImageType::e2D)
    .setMipLevels (mip_levels)
    .setArrayLayers (1)
    .setFormat (format)
    .setTiling (tiling)
    .setInitialLayout (vk::ImageLayout::eUndefined)
    .setUsage (usage)
    .setSharingMode (vk::SharingMode::eExclusive)
    .setSamples (num_samples)
    .setExtent(vk::Extent3D ()
	       .setWidth (width)
	       .setHeight (height)
	       .setDepth (1));

  if (m_logical_device.createImage (&image_info, nullptr, &image) != vk::Result::eSuccess)
    {
      throw std::runtime_error("Failed to create image.");
    }

  vk::MemoryRequirements mem_requirements;
  m_logical_device.getImageMemoryRequirements (image, &mem_requirements);

  vk::MemoryAllocateInfo alloc_info = vk::MemoryAllocateInfo ()
    .setAllocationSize (mem_requirements.size)
    .setMemoryTypeIndex (m_find_memory_type (mem_requirements.memoryTypeBits, properties));

  if (m_logical_device.allocateMemory (&alloc_info, nullptr, &image_memory) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to allocate image memory!");
    }

  m_logical_device.bindImageMemory (image, image_memory, 0);
}
