/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_render_pass ()
{
  vk::AttachmentDescription color_attachment = vk::AttachmentDescription ()
    .setFormat (m_swap_chain_image_format)
    .setSamples (m_msaa_samples)
    .setLoadOp (vk::AttachmentLoadOp::eClear)
    .setStoreOp (vk::AttachmentStoreOp::eStore)
    .setStencilLoadOp (vk::AttachmentLoadOp::eDontCare)
    .setStencilStoreOp (vk::AttachmentStoreOp::eDontCare)
    .setInitialLayout (vk::ImageLayout::eUndefined)
    .setFinalLayout (vk::ImageLayout::eColorAttachmentOptimal);

  vk::AttachmentDescription depth_attachment = vk::AttachmentDescription ()
    .setFormat (m_find_depth_format ())
    .setSamples (m_msaa_samples)
    .setLoadOp (vk::AttachmentLoadOp::eClear)
    .setStoreOp (vk::AttachmentStoreOp::eDontCare)
    .setStencilLoadOp (vk::AttachmentLoadOp::eDontCare)
    .setStencilStoreOp (vk::AttachmentStoreOp::eDontCare)
    .setInitialLayout (vk::ImageLayout::eUndefined)
    .setFinalLayout (vk::ImageLayout::eDepthStencilAttachmentOptimal);

  vk::AttachmentDescription color_attachment_resolve = vk::AttachmentDescription ()
    .setFormat (m_swap_chain_image_format)
    .setSamples (vk::SampleCountFlagBits::e1)
    .setLoadOp (vk::AttachmentLoadOp::eDontCare)
    .setStoreOp (vk::AttachmentStoreOp::eStore)
    .setStencilLoadOp (vk::AttachmentLoadOp::eDontCare)
    .setStencilStoreOp (vk::AttachmentStoreOp::eDontCare)
    .setInitialLayout (vk::ImageLayout::eUndefined)
    .setFinalLayout (vk::ImageLayout::ePresentSrcKHR);

  vk::AttachmentReference color_attachment_ref = vk::AttachmentReference ()
    .setAttachment (0)
    .setLayout (vk::ImageLayout::eColorAttachmentOptimal);

  vk::AttachmentReference depth_attachment_ref = vk::AttachmentReference ()
    .setAttachment (1)
    .setLayout (vk::ImageLayout::eDepthStencilAttachmentOptimal);

  vk::AttachmentReference color_attachment_resolve_ref = vk::AttachmentReference ()
    .setAttachment (2)
    .setLayout (vk::ImageLayout::eColorAttachmentOptimal);

  vk::SubpassDescription subpass = vk::SubpassDescription ()
    .setPipelineBindPoint (vk::PipelineBindPoint::eGraphics)
    .setColorAttachmentCount (1)
    .setPColorAttachments (&color_attachment_ref)
    .setPDepthStencilAttachment (&depth_attachment_ref)
    .setPResolveAttachments (&color_attachment_resolve_ref);

  vk::SubpassDependency dependency = vk::SubpassDependency ()
    .setSrcSubpass (VK_SUBPASS_EXTERNAL)
    .setDstSubpass (0)
    .setSrcStageMask (vk::PipelineStageFlagBits::eColorAttachmentOutput)
    .setSrcAccessMask (vk::AccessFlagBits::eMemoryRead)
    .setDstStageMask (vk::PipelineStageFlagBits::eColorAttachmentOutput)
    .setDstAccessMask (vk::AccessFlagBits::eColorAttachmentRead |
		       vk::AccessFlagBits::eColorAttachmentWrite);

  std::array<vk::AttachmentDescription, 3> attachments = {color_attachment, depth_attachment, color_attachment_resolve};
  vk::RenderPassCreateInfo render_pass_info = vk::RenderPassCreateInfo ()
    .setAttachmentCount (static_cast<uint32_t> (attachments.size ()))
    .setPAttachments (attachments.data ())
    .setSubpassCount (1)
    .setPSubpasses (&subpass)
    .setDependencyCount (1)
    .setPDependencies (&dependency);

  if (m_logical_device.createRenderPass (&render_pass_info, nullptr, &m_render_pass) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to crteate render pass.");
    }
}
