/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

app::vulkan::queue_family_indices
app::vulkan::m_find_queue_families (vk::PhysicalDevice physical_device)
{
  queue_family_indices indices;

  uint32_t queue_family_count = 0;
  vkGetPhysicalDeviceQueueFamilyProperties (physical_device, &queue_family_count, nullptr);
  std::vector<vk::QueueFamilyProperties> queue_families (queue_family_count);
  physical_device.getQueueFamilyProperties (&queue_family_count, queue_families.data ());

  int i = 0;
  for (const auto& queue_family : queue_families)
    {
      if (queue_family.queueFlags & vk::QueueFlagBits::eGraphics)
	{
	  indices.graphics_family = i;
	}

      vk::Bool32 present_support = false;
      physical_device.getSurfaceSupportKHR (i, m_surface, &present_support);

      if (present_support)
	{
	  indices.present_family = i;
	}

      if (indices.is_complete ())
	{
	  break;
	}
      
      ++i;
    }
  
  return indices;
}
