/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_draw_frame ()
{
  m_logical_device.waitForFences (1, &m_in_flight_fences[m_current_frame], VK_TRUE, UINT64_MAX);
  
  uint32_t image_index;
  vk::Result result = m_logical_device.acquireNextImageKHR (m_swap_chain, UINT64_MAX, m_image_available_semaphores[m_current_frame], nullptr, &image_index);

  if (result == vk::Result::eErrorOutOfDateKHR || result == vk::Result::eSuboptimalKHR)
    {
      m_recreate_swap_chain ();
      return;
    }
  else if (result != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to acquire swap chain image!");
    }

  m_update_uniform_buffer (image_index);

  if (m_images_in_flight[image_index])
    {
      m_logical_device.waitForFences (1, &m_images_in_flight[image_index], VK_TRUE, UINT64_MAX);
    }
  m_images_in_flight[image_index] = m_in_flight_fences[m_current_frame];

  vk::Semaphore wait_semaphores[] = {m_image_available_semaphores[m_current_frame]};
  vk::Semaphore signal_semaphores[] = {m_render_finished_semaphores[m_current_frame]};
  vk::PipelineStageFlags wait_stages[] = {vk::PipelineStageFlagBits::eColorAttachmentOutput};
    
  vk::SubmitInfo submit_info = vk::SubmitInfo ()
    .setWaitSemaphoreCount (1)
    .setPWaitSemaphores (wait_semaphores)
    .setPWaitDstStageMask (wait_stages)
    .setCommandBufferCount (1)
    .setPCommandBuffers (&m_command_buffers[image_index])
    .setSignalSemaphoreCount (1)
    .setPSignalSemaphores (signal_semaphores);

  m_logical_device.resetFences (1, &m_in_flight_fences[m_current_frame]);

  if (m_graphics_queue.submit (1, &submit_info, m_in_flight_fences[m_current_frame]) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to submit draw command buffer.");
    }

    vk::SwapchainKHR swap_chains[] = {m_swap_chain};
  
  vk::PresentInfoKHR present_info = vk::PresentInfoKHR ()
    .setWaitSemaphoreCount (1)
    .setPWaitSemaphores (signal_semaphores)
    .setSwapchainCount (1)
    .setPSwapchains (swap_chains)
    .setPImageIndices (&image_index);
  
  m_present_queue.presentKHR (&present_info);
  
  m_present_queue.waitIdle ();

  m_current_frame = (m_current_frame + 1) % m_max_frames_in_flight;
}
