/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

vk::SampleCountFlagBits
app::vulkan::m_get_max_usable_smaple_count ()
{
  vk::PhysicalDeviceProperties physical_device_properties;
  m_physical_device.getProperties (&physical_device_properties);

  vk::SampleCountFlags counts = physical_device_properties.limits.framebufferColorSampleCounts & physical_device_properties.limits.framebufferDepthSampleCounts;

  if (counts & vk::SampleCountFlagBits::e64) {return vk::SampleCountFlagBits::e64;}
  if (counts & vk::SampleCountFlagBits::e32) {return vk::SampleCountFlagBits::e32;}
  if (counts & vk::SampleCountFlagBits::e16) {return vk::SampleCountFlagBits::e16;}
  if (counts & vk::SampleCountFlagBits::e8) {return vk::SampleCountFlagBits::e8;}
  if (counts & vk::SampleCountFlagBits::e4) {return vk::SampleCountFlagBits::e4;}
  if (counts & vk::SampleCountFlagBits::e2) {return vk::SampleCountFlagBits::e2;}

  return vk::SampleCountFlagBits::e1;
}
