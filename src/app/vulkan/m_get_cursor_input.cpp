/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_get_cursor_input (GLFWwindow* window)
{
  // Cursor:
  {
    double x_pos, y_pos;
    glfwGetCursorPos (window, &x_pos, &y_pos);

    if (m_first_cursor)
      {
	m_last_x = x_pos;
	m_last_y = y_pos;
	m_first_cursor = false;
      }

    double x_offset = x_pos - m_last_x;
    double y_offset = m_last_y - y_pos;
    m_last_x = x_pos;
    m_last_y = y_pos;

    float sensitivity = 0.1f;
    x_offset *= -sensitivity;
    y_offset *= sensitivity;

    m_yaw += x_offset;
    m_pitch += y_offset;
  }

  
  // Gamepad:
  /*
  {
    glfwGetGamepadState(GLFW_JOYSTICK_1, &state);

    double sensitivity = 150.0f;
  
    m_yaw -= state.axes[GLFW_GAMEPAD_AXIS_RIGHT_X] * m_delta_time * sensitivity;
    m_pitch -= state.axes[GLFW_GAMEPAD_AXIS_RIGHT_Y] * m_delta_time * sensitivity;
  }
  */

  // Prevent wrapping of pitch.
  if (m_pitch > 89.0f)
    {
      m_pitch = 89.0f;
    }
  if (m_pitch < -89.0f)
    {
      m_pitch = -89.0f;
    }

  // Limit value of m_yaw.
  if (m_yaw < -180.0f)
    {
      m_yaw += 360.0f;
    }
    if (m_yaw > 180.0f)
    {
      m_yaw -= 360.0f;
    }
  
  glm::vec3 front;
  front.x = cos (glm::radians (m_yaw)) * cos (glm::radians (m_pitch));
  front.y = sin (glm::radians (m_yaw)) * cos (glm::radians (m_pitch));
  front.z = sin (glm::radians (m_pitch)); 
  m_camera_front = front;
}
