/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

bool
app::vulkan::m_check_device_extension_support (const vk::PhysicalDevice& physical_device)
{
  std::vector<vk::ExtensionProperties> available_extensions =
    m_get_available_device_extensions(physical_device);
  std::vector<const char*> required_extensions_cstr = m_get_required_device_extensions ();
  std::set<std::string> required_extensions (required_extensions_cstr.begin (),
					     required_extensions_cstr.end ());

  for (const auto& extension : available_extensions)
    {
      required_extensions.erase (extension.extensionName);
    }

  return required_extensions.empty ();
}
