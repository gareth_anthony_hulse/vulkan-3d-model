/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_transition_image_layout (vk::Image image, vk::Format /*format*/, vk::ImageLayout old_layout, vk::ImageLayout new_layout, uint32_t mip_levels)
{
  vk::CommandBuffer command_buffer = m_begin_single_time_commands ();

  vk::ImageMemoryBarrier barrier = vk::ImageMemoryBarrier ()
    .setOldLayout (old_layout)
    .setNewLayout (new_layout)
    .setSrcQueueFamilyIndex (VK_QUEUE_FAMILY_IGNORED)
    .setDstQueueFamilyIndex (VK_QUEUE_FAMILY_IGNORED)
    .setImage (image)
    .setSubresourceRange (vk::ImageSubresourceRange ()
			  .setAspectMask (vk::ImageAspectFlagBits::eColor)
			  .setBaseMipLevel (0)
			  .setLevelCount (mip_levels)
			  .setBaseArrayLayer (0)
			  .setLayerCount (1));

  vk::PipelineStageFlags src_stage;
  vk::PipelineStageFlags dst_stage;

  if (old_layout == vk::ImageLayout::eUndefined && new_layout == vk::ImageLayout::eTransferDstOptimal)
    {
      //      barrier.setSrcAccessMask (0);
      barrier.setDstAccessMask (vk::AccessFlagBits::eTransferWrite);

      src_stage = vk::PipelineStageFlagBits::eTopOfPipe;
      dst_stage = vk::PipelineStageFlagBits::eTransfer;
    }
  else if (old_layout == vk::ImageLayout::eTransferDstOptimal && new_layout == vk::ImageLayout::eShaderReadOnlyOptimal)
    {
      barrier.setSrcAccessMask (vk::AccessFlagBits::eTransferWrite);
      barrier.setDstAccessMask (vk::AccessFlagBits::eShaderRead);

      src_stage = vk::PipelineStageFlagBits::eTransfer;
      dst_stage = vk::PipelineStageFlagBits::eFragmentShader;
    }
  else
    {
      throw std::invalid_argument ("Unsupported layout transition.");
    }

  command_buffer.pipelineBarrier (src_stage, dst_stage, vk::DependencyFlags (0),	       
				  0, nullptr,
				  0, nullptr,
				  1, &barrier);

  m_end_single_time_commands (command_buffer);
}
