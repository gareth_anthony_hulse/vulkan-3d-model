/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_cleanup_swap_chain ()
{
  m_logical_device.destroyImageView (m_depth_image_view, nullptr);
  m_logical_device.destroyImage (m_depth_image, nullptr);
  m_logical_device.freeMemory (m_depth_image_memory, nullptr);
  m_logical_device.destroyImageView (m_color_image_view, nullptr);
  m_logical_device.destroyImage (m_color_image, nullptr);
  m_logical_device.freeMemory (m_color_image_memory, nullptr);
  
  for (auto framebuffer : m_swap_chain_framebuffers)
    {
      m_logical_device.destroyFramebuffer (framebuffer, nullptr);
    }
  
  m_logical_device.destroyPipeline (m_graphics_pipeline, nullptr);
  m_logical_device.destroyPipelineLayout (m_pipeline_layout, nullptr);
  m_logical_device.destroyRenderPass ( m_render_pass, nullptr);
  
  for (auto image_view: m_swap_chain_image_views)
    {
      m_logical_device.destroyImageView (image_view, nullptr);
    }
  
  m_logical_device.destroySwapchainKHR (m_swap_chain, nullptr);

  for (size_t i = 0; i < m_swap_chain_images.size (); ++i)
    {
      m_logical_device.destroyBuffer (m_uniform_buffers[i], nullptr);
      m_logical_device.freeMemory (m_uniform_buffers_memory[i], nullptr);
    }

  m_logical_device.destroyDescriptorPool (m_descriptor_pool, nullptr);
}
