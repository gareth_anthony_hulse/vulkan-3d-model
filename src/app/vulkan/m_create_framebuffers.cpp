/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_framebuffers ()
{
  m_swap_chain_framebuffers.resize (m_swap_chain_image_views.size ());

  for (size_t i = 0; i < m_swap_chain_image_views.size (); ++i)
    {
      std::array<vk::ImageView, 3> attachments =
	{
	 m_color_image_view,
	 m_depth_image_view,
	 m_swap_chain_image_views[i]
	};

      vk::FramebufferCreateInfo framebuffer_info = vk::FramebufferCreateInfo ()
	.setRenderPass (m_render_pass)
	.setAttachmentCount (static_cast<uint32_t> (attachments.size ()))
	.setPAttachments (attachments.data ())
	.setWidth (m_swap_chain_extent.width)
	.setHeight (m_swap_chain_extent.height)
	.setLayers (1);

	if (m_logical_device.createFramebuffer (&framebuffer_info, nullptr,
				 &m_swap_chain_framebuffers[i]) != vk::Result::eSuccess)
	  {
	    throw std::runtime_error ("Failed to create framebuffer.");
	  }
    };
}
