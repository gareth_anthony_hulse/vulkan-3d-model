/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_sync_objects ()
{
  m_image_available_semaphores.resize (m_max_frames_in_flight);
  m_render_finished_semaphores.resize (m_max_frames_in_flight);
  m_in_flight_fences.resize (m_max_frames_in_flight);
  m_images_in_flight.resize (m_swap_chain_images.size (), nullptr);
  
  vk::SemaphoreCreateInfo semaphore_info = vk::SemaphoreCreateInfo ();

  vk::FenceCreateInfo fence_info = vk::FenceCreateInfo ()
    .setFlags (vk::FenceCreateFlagBits::eSignaled);

  for (size_t i = 0; i < m_max_frames_in_flight; ++i)
    {
      if (m_logical_device.createSemaphore (&semaphore_info,
					    nullptr,
					    &m_image_available_semaphores[i]) != vk::Result::eSuccess ||
	  m_logical_device.createSemaphore (&semaphore_info,
					    nullptr,
					    &m_render_finished_semaphores[i]) != vk::Result::eSuccess ||
	  m_logical_device.createFence (&fence_info,
					nullptr,
					&m_in_flight_fences[i]) != vk::Result::eSuccess)
	{
	  throw std::runtime_error ("Failed to create synchronization objects for a frame.");
	}
    }
}
