/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_descriptor_sets ()
{
  std::vector<vk::DescriptorSetLayout> layouts (m_swap_chain_images.size (), m_descriptor_set_layout);
  vk::DescriptorSetAllocateInfo alloc_info = vk::DescriptorSetAllocateInfo ()
    .setDescriptorPool (m_descriptor_pool)
    .setDescriptorSetCount (static_cast<uint32_t> (m_swap_chain_images.size ()))
    .setPSetLayouts (layouts.data ());

  m_descriptor_sets.resize (m_swap_chain_images.size ());
  if (m_logical_device.allocateDescriptorSets (&alloc_info, m_descriptor_sets.data ()) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to allocate descriptor sets.");
    }

  for (size_t i = 0; i < m_swap_chain_images.size (); ++i)
    {
      vk::DescriptorBufferInfo buffer_info = vk::DescriptorBufferInfo ()
	.setBuffer (m_uniform_buffers[i])
	.setOffset (0)
	.setRange (sizeof (app::vulkan::uniform_buffer_object));
      
      vk::DescriptorImageInfo image_info = vk::DescriptorImageInfo ()
	.setImageLayout (vk::ImageLayout::eShaderReadOnlyOptimal)
	.setImageView (m_texture_image_view)
	.setSampler (m_texture_sampler);

      std::array<vk::WriteDescriptorSet, 2> descriptor_writes = {};
      descriptor_writes[0].setDstSet (m_descriptor_sets[i]);
      descriptor_writes[0].setDstBinding (0);
      descriptor_writes[0].setDstArrayElement (0);
      descriptor_writes[0].setDescriptorType (vk::DescriptorType::eUniformBuffer);
      descriptor_writes[0].setDescriptorCount (1);
      descriptor_writes[0].setPBufferInfo (&buffer_info);
      
      descriptor_writes[1].setDstSet (m_descriptor_sets[i]);
      descriptor_writes[1].setDstBinding (1);
      descriptor_writes[1].setDstArrayElement (0);
      descriptor_writes[1].setDescriptorType (vk::DescriptorType::eCombinedImageSampler);
      descriptor_writes[1].setDescriptorCount (1);
      descriptor_writes[1].setPImageInfo (&image_info);

      m_logical_device.updateDescriptorSets (static_cast<uint32_t> (descriptor_writes.size ()), descriptor_writes.data (), 0, nullptr);
    }
}
