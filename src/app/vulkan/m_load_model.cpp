/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define TINYOBJLOADER_IMPLMENTATION

#include "../vulkan.hpp"

void
app::vulkan::m_load_model ()
{
  tinyobj::attrib_t attrib;
  std::vector<tinyobj::shape_t> shapes;
  std::vector<tinyobj::material_t> materials;
  std::string warn, err;

  if (!tinyobj::LoadObj (&attrib, &shapes, &materials, &warn, &err, PKGDATADIR "/models/chalet.obj"))
    {
      throw std::runtime_error (warn + err);
    }

  m_bind_model (&attrib, &shapes);
}
