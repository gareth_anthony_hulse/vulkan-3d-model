/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_logical_device ()
{
  queue_family_indices indices = m_find_queue_families (m_physical_device);

  std::vector<vk::DeviceQueueCreateInfo> logical_device_queue_create_infos;
  std::set<uint32_t> unique_queue_families =
    {
     indices.graphics_family.value (),
     indices.present_family.value ()
    };

  float queue_priority = 1.0f;
  for (uint32_t queue_family : unique_queue_families)
    {
      logical_device_queue_create_infos.push_back(vk::DeviceQueueCreateInfo ()
						  .setQueueFamilyIndex (queue_family)
						  .setQueueCount (1)
						  .setPQueuePriorities (&queue_priority));
    }

  vk::PhysicalDeviceFeatures physical_device_features = vk::PhysicalDeviceFeatures ()
    .setSamplerAnisotropy (VK_TRUE)
    .setSampleRateShading (VK_TRUE)
    .setAlphaToOne (VK_TRUE);

  vk::DeviceCreateInfo logical_device_create_info = vk::DeviceCreateInfo ()
    .setQueueCreateInfoCount (logical_device_queue_create_infos.size ())
    .setPQueueCreateInfos (logical_device_queue_create_infos.data ())
    .setPEnabledFeatures (&physical_device_features)
    .setEnabledExtensionCount (m_device_extensions.size ())
    .setPpEnabledExtensionNames (m_device_extensions.data ());

  if (m_physical_device.createDevice (&logical_device_create_info, nullptr, &m_logical_device)
      != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create logical device.");
    }

  m_logical_device.getQueue (indices.graphics_family.value (), 0, &m_graphics_queue);
  m_logical_device.getQueue (indices.present_family.value (), 0, &m_present_queue);
}
