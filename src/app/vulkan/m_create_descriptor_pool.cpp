/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_descriptor_pool ()
{
  std::array<vk::DescriptorPoolSize, 2> pool_sizes = {};
  pool_sizes[0].type = vk::DescriptorType::eUniformBuffer;
  pool_sizes[0].descriptorCount = static_cast<uint32_t> (m_swap_chain_images.size ());
  pool_sizes[1].type = vk::DescriptorType::eCombinedImageSampler;
  pool_sizes[1].descriptorCount = static_cast<uint32_t> (m_swap_chain_images.size ());

  vk::DescriptorPoolCreateInfo pool_info = vk::DescriptorPoolCreateInfo ()
    .setPoolSizeCount (static_cast<uint32_t> (pool_sizes.size ()))
    .setPPoolSizes (pool_sizes.data ())
    .setMaxSets (static_cast<uint32_t> (m_swap_chain_images.size ()));

  if (m_logical_device.createDescriptorPool (&pool_info, nullptr, &m_descriptor_pool) != vk::Result::eSuccess)
    {
      throw std::runtime_error("Failed to create descriptor pool.");
    }
}
