/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_color_resources ()
{
  vk::Format color_format = m_swap_chain_image_format;

  m_create_image (m_swap_chain_extent.width, m_swap_chain_extent.height, 1, m_msaa_samples, color_format, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment,
		  vk::MemoryPropertyFlagBits::eDeviceLocal, m_color_image, m_color_image_memory);

  m_color_image_view = m_create_image_view (m_color_image, color_format, vk::ImageAspectFlagBits::eColor, 1);
}
