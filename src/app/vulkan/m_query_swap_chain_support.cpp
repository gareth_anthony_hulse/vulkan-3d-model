/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

app::vulkan::swap_chain_support_details
app::vulkan::m_query_swap_chain_support (vk::PhysicalDevice physical_device)
{
  swap_chain_support_details details;

  physical_device.getSurfaceCapabilitiesKHR (m_surface, &details.capabilities);

  uint32_t format_count;
  physical_device.getSurfaceFormatsKHR (m_surface, &format_count, details.formats.data ());

  if (format_count != 0)
    {
      details.formats.resize (format_count);
      physical_device.getSurfaceFormatsKHR (m_surface, &format_count, details.formats.data ());
    }

  uint32_t present_mode_count;
  physical_device.getSurfacePresentModesKHR (m_surface, &present_mode_count, details.present_modes.data ());

  if (present_mode_count != 0)
    {
      details.present_modes.resize (present_mode_count);
        physical_device.getSurfacePresentModesKHR (m_surface, &present_mode_count, details.present_modes.data ());
    }

  return details;
}
