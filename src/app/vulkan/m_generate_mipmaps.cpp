/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_generate_mipmaps (vk::Image image, vk::Format image_format, int32_t tex_width, int32_t tex_height, uint32_t mip_levels)
{
  vk::FormatProperties format_properties;
  m_physical_device.getFormatProperties (image_format, &format_properties);

  if (!(format_properties.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear))
    {
      throw std::runtime_error ("Texture image format does not support linear blitting.");
    }
  
  vk::CommandBuffer command_buffer = m_begin_single_time_commands ();

  vk::ImageSubresourceRange subres = vk::ImageSubresourceRange ()
    .setAspectMask (vk::ImageAspectFlagBits::eColor)
    .setBaseArrayLayer (0)
    .setLayerCount (1)
    .setLevelCount (1);

  vk::ImageMemoryBarrier barrier = vk::ImageMemoryBarrier ()
    .setImage (image)
    .setSrcQueueFamilyIndex (VK_QUEUE_FAMILY_IGNORED)
    .setDstQueueFamilyIndex (VK_QUEUE_FAMILY_IGNORED);
    

  int32_t mip_width = tex_width;
  int32_t mip_height = tex_height;

  for (uint32_t i = 1; i < mip_levels; ++i)
    {
      barrier.setSubresourceRange (subres.setBaseMipLevel (i - 1));
      
      barrier.setOldLayout (vk::ImageLayout::eTransferDstOptimal);
      barrier.setNewLayout (vk::ImageLayout::eTransferSrcOptimal);
      barrier.setSrcAccessMask (vk::AccessFlagBits::eTransferWrite);
      barrier.setDstAccessMask (vk::AccessFlagBits::eTransferRead);

      command_buffer.pipelineBarrier (vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eTransfer, vk::DependencyFlags (0),
				      0, nullptr,
				      0, nullptr,
				      1, &barrier);

      // Souce offsets:
      std::array<vk::Offset3D, 2> src_offsets;
      src_offsets[0] = vk::Offset3D (0, 0, 0);
      src_offsets[1] = vk::Offset3D (mip_width, mip_height, 1);

      // Desination offsets:
      std::array<vk::Offset3D, 2> dst_offsets;
      dst_offsets[0] = vk::Offset3D (0, 0, 0);
      dst_offsets[1] = vk::Offset3D (mip_width > 1 ? mip_width / 2 : 1, mip_height > 1 ? mip_height / 2 : 1, 1);
      
      vk::ImageBlit blit = vk::ImageBlit ()
	.setSrcOffsets (src_offsets)
	.setSrcSubresource (vk::ImageSubresourceLayers ()
			    .setAspectMask (vk::ImageAspectFlagBits::eColor)
			    .setMipLevel (i - 1)
			    .setBaseArrayLayer (0)
			    .setLayerCount (1))
	.setDstOffsets (dst_offsets)
	.setDstSubresource (vk::ImageSubresourceLayers ()
			    .setAspectMask (vk::ImageAspectFlagBits::eColor)
			    .setMipLevel (i)
			    .setBaseArrayLayer (0)
			    .setLayerCount (1));

      command_buffer.blitImage (image, vk::ImageLayout::eTransferSrcOptimal,
				image, vk::ImageLayout::eTransferDstOptimal,
				1, &blit,
				vk::Filter::eLinear);

      barrier.setOldLayout (vk::ImageLayout::eTransferSrcOptimal);
      barrier.setNewLayout (vk::ImageLayout::eShaderReadOnlyOptimal);
      barrier.setSrcAccessMask (vk::AccessFlagBits::eTransferRead);
      barrier.setDstAccessMask (vk::AccessFlagBits::eShaderRead);

      command_buffer.pipelineBarrier (vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, vk::DependencyFlags (0),
				      0, nullptr,
				      0, nullptr,
				      1, &barrier);
      
      if (mip_width > 1)
	{
	  mip_width /= 2;
	}
      if (mip_height > 1)
	{
	  mip_height /= 2;
	}
    }

  barrier.setSubresourceRange (subres.setBaseMipLevel (mip_levels - 1));
  barrier.setOldLayout (vk::ImageLayout::eTransferDstOptimal);
  barrier.setNewLayout (vk::ImageLayout::eShaderReadOnlyOptimal);
  barrier.setSrcAccessMask (vk::AccessFlagBits::eTransferWrite);
  barrier.setDstAccessMask (vk::AccessFlagBits::eShaderRead);

  command_buffer.pipelineBarrier (vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, vk::DependencyFlags (0),
				     0, nullptr,
				     0, nullptr,
				     1, &barrier);

  m_end_single_time_commands (command_buffer);
}
