/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_get_key_input (GLFWwindow* window)
{
    float velocity = 2.0f * m_delta_time;
  
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
      glfwSetWindowShouldClose(window, true);
    }

  if (glfwGetKey(window, GLFW_KEY_W) != GLFW_RELEASE)
    {
      m_camera_pos += velocity * m_camera_front;
    }
  if (glfwGetKey(window, GLFW_KEY_A) != GLFW_RELEASE)
    {
      m_camera_pos -= glm::normalize(glm::cross(m_camera_front, m_camera_up)) * velocity;
    }
  if (glfwGetKey(window, GLFW_KEY_S) != GLFW_RELEASE)
    {
      m_camera_pos -= velocity * m_camera_front;
    }
  if (glfwGetKey(window, GLFW_KEY_D) != GLFW_RELEASE)
    {
      m_camera_pos += glm::normalize(glm::cross(m_camera_front, m_camera_up)) * velocity;
    }
  if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) != GLFW_RELEASE)
    {
      m_camera_pos.z -= m_camera_up.z * velocity;
    }
  if (glfwGetKey(window, GLFW_KEY_SPACE) != GLFW_RELEASE)
    {
      m_camera_pos.z += m_camera_up.z * velocity;
    }

  // Gamepad:
  /*
  glfwGetGamepadState(GLFW_JOYSTICK_1, &state);
  m_camera_pos += glm::normalize(glm::cross(m_camera_front, m_camera_up)) *
    (velocity * state.axes[GLFW_GAMEPAD_AXIS_LEFT_X]);
  m_camera_pos -= (velocity * state.axes[GLFW_GAMEPAD_AXIS_LEFT_Y]) * m_camera_front;
  */
}
