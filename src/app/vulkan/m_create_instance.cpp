/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_create_instance ()
{
#ifndef NDEBUG
  if (!m_check_validation_layer_support ())
    {
      throw std::runtime_error ("Validition layers not available!");
    }

  vk::DebugUtilsMessengerCreateInfoEXT debug_create_info;
  m_populate_debug_messenger_create_info (debug_create_info);
#endif // NDEBUG
  
  vk::ApplicationInfo app_info = vk::ApplicationInfo ()
    .setPApplicationName (PACKAGE_NAME)
    .setApplicationVersion (1)
    .setPEngineName ("N/A")
    .setEngineVersion (1)
    .setApiVersion (VK_API_VERSION_1_2);
  
  auto instance_extensions = m_get_required_instance_exts ();
  
  vk::InstanceCreateInfo create_info = vk::InstanceCreateInfo ()
    .setPApplicationInfo (&app_info)
    .setEnabledExtensionCount (instance_extensions.size ())
    .setPpEnabledExtensionNames (instance_extensions.data ())
#ifndef NDEBUG
  .setEnabledLayerCount (static_cast<uint32_t> (m_validation_layers.size ()))
  .setPpEnabledLayerNames (m_validation_layers.data ())
  .setPNext (static_cast<vk::DebugUtilsMessengerCreateInfoEXT*> (&debug_create_info))
 #else
  .setEnabledLayerCount (0)
  .setPNext (nullptr)
#endif // NDEBUG
    ;
  
  if (vk::createInstance (&create_info, nullptr, &m_instance) != vk::Result::eSuccess)
    {
      throw std::runtime_error ("Failed to create instance!");
    }
}
