/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

#include <unordered_map>

void
app::vulkan::m_bind_model (tinyobj::attrib_t* attrib, std::vector<tinyobj::shape_t>* shapes)
{
  std::unordered_map<app::vulkan::vertex, uint32_t> unique_vertices = {};
  
  for (const auto& shape : *shapes)
    {
      for (const auto& index : shape.mesh.indices)
	{
	  app::vulkan::vertex my_vertex = {};

	  my_vertex.pos =
	    {
	     attrib->vertices[3 * index.vertex_index + 0],
	     attrib->vertices[3 * index.vertex_index + 1],
	     attrib->vertices[3 * index.vertex_index + 2]
	    };

	  my_vertex.tex_coord =
	    {
	     attrib->texcoords[2 * index.texcoord_index + 0],
	     1.0f - attrib->texcoords[2 * index.texcoord_index + 1]
	    };
	  
	  my_vertex.color = {1.0f, 1.0f, 1.0f};

	  if (unique_vertices.count (my_vertex) == 0)
	    {
	      unique_vertices[my_vertex] = static_cast<uint32_t> (m_vertices.size ());
	      m_vertices.push_back (my_vertex);
	    }
	  
	  m_indicies.push_back (unique_vertices[my_vertex]);
	}
    }
}
