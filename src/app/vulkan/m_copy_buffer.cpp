/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::m_copy_buffer (vk::Buffer src_buffer, vk::Buffer dst_buffer, vk::DeviceSize size)
{
  vk::CommandBuffer command_buffer = m_begin_single_time_commands ();

  vk::BufferCopy copy_region = vk::BufferCopy ()
    .setSize (size);
  command_buffer.copyBuffer (src_buffer, dst_buffer, 1, &copy_region);

  m_end_single_time_commands (command_buffer);
}
