/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef APP_VULKAN_HPP
#define APP_VULKAN_HPP

#define GLFW_INCLUDE_NONE
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <vulkan/vulkan.hpp>
#include <GLFW/glfw3.h>
#include <cstdio>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <set>
#include <cstring>
#include <optional>
#include <cstdint>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>
#include <array>
#include <chrono>
#include <thread>
#include <tiny_obj_loader.h>

namespace app
{ 
  class vulkan
  {
      public:
    // Structs:
    struct vertex
    {
      glm::vec3 pos;
      glm::vec3 color;
      glm::vec2 tex_coord;

      static vk::VertexInputBindingDescription get_binding_description ();
      static std::array<vk::VertexInputAttributeDescription, 3> get_attribute_descriptions ();
      
      bool operator== (const vertex& test_vertex) const
      {
	return
	  pos == test_vertex.pos &&
	  color == test_vertex.color &&
	  tex_coord == test_vertex.tex_coord;
      }
    };
    
    // Functions:
    ~vulkan ();
    
    void run ();
    
  private:
    // Structs:
    struct queue_family_indices
    {
      std::optional<uint32_t> graphics_family;
      std::optional<uint32_t> present_family;

      bool is_complete ();
    };

    struct swap_chain_support_details
    {
      vk::SurfaceCapabilitiesKHR capabilities;
      std::vector<vk::SurfaceFormatKHR> formats;
      std::vector<vk::PresentModeKHR> present_modes;
    };

    struct uniform_buffer_object
    {
      alignas (16) glm::mat4 model;
      alignas (16) glm::mat4 view;
      alignas (16) glm::mat4 proj;
    };
    
    // Variables:
#ifndef NDEBUG
    const std::vector<const char*>
    m_validation_layers =
      {
       "VK_LAYER_KHRONOS_validation"
      };
    
    VkDebugUtilsMessengerEXT m_debug_messenger;
#endif // NDEBUG
    
    const std::vector<const char*>
    m_device_extensions =
      {
       VK_KHR_SWAPCHAIN_EXTENSION_NAME
      };
    
    std::vector<app::vulkan::vertex> m_vertices;
    std::vector<uint32_t> m_indicies;  
    GLFWwindow* m_window;
    vk::Instance m_instance;
    vk::PhysicalDevice m_physical_device;
    vk::Device m_logical_device;
    vk::Queue m_graphics_queue;
    vk::Queue m_present_queue;
    VkSurfaceKHR m_surface;
    const std::string m_target_session_type = "wayland";
    vk::SwapchainKHR m_swap_chain;
    std::vector<vk::Image> m_swap_chain_images;
    vk::Format m_swap_chain_image_format;
    vk::Extent2D m_swap_chain_extent;
    std::vector<vk::ImageView> m_swap_chain_image_views;
    vk::RenderPass m_render_pass;
    vk::PipelineLayout m_pipeline_layout;
    vk::Pipeline m_graphics_pipeline;
    std::vector<vk::Framebuffer> m_swap_chain_framebuffers;
    vk::CommandPool m_command_pool;
    std::vector<vk::CommandBuffer> m_command_buffers;
    std::vector<vk::Semaphore> m_image_available_semaphores;
    std::vector<vk::Semaphore> m_render_finished_semaphores;
    std::vector<vk::Fence> m_in_flight_fences;
    std::vector<vk::Fence> m_images_in_flight;
    const uint m_max_frames_in_flight = 2;
    size_t m_current_frame = 0;
    bool m_framebuffer_resized = false;
    vk::Buffer m_vertex_buffer;
    vk::DeviceMemory m_vertex_buffer_memory;
    vk::Buffer m_index_buffer;
    vk::DeviceMemory m_index_buffer_memory;
    vk::DescriptorSetLayout m_descriptor_set_layout;
    std::vector<vk::Buffer> m_uniform_buffers;
    std::vector<vk::DeviceMemory> m_uniform_buffers_memory;
    vk::DescriptorPool m_descriptor_pool;
    std::vector<vk::DescriptorSet> m_descriptor_sets;
    vk::Image m_texture_image;
    vk::DeviceMemory m_texture_image_memory;
    vk::ImageView m_texture_image_view;
    vk::Sampler m_texture_sampler;
    std::chrono::duration<double> m_refresh_rate;
    vk::Image m_depth_image;
    vk::DeviceMemory m_depth_image_memory;
    vk::ImageView m_depth_image_view;
    uint32_t m_mip_levels;
    vk::SampleCountFlagBits m_msaa_samples = vk::SampleCountFlagBits::e1;
    vk::Image m_color_image;
    vk::DeviceMemory m_color_image_memory;
    vk::ImageView m_color_image_view;
    glm::vec3 m_camera_pos = glm::vec3 (0.0f, 3.5f, 0.0f);
    glm::vec3 m_camera_front = glm::vec3 (0.0f, 0.0f, 0.0f);
    glm::vec3 m_camera_up = glm::vec3 (0.0f, 0.0f, 1.0f);
    double m_delta_time = 0.0f;
    double m_yaw = -90.0f;
    double m_pitch = 0.0f;
    double m_last_x;
    double m_last_y;
    bool m_first_cursor = true;
    GLFWgamepadstate state;
    
    // Functions:
#ifndef NDEBUG
    bool m_check_validation_layer_support ();
    static VKAPI_ATTR VkBool32 VKAPI_CALL
    m_debug_callback (VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
		       VkDebugUtilsMessageTypeFlagsEXT message_type,
		       const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
		       void* user_data);
    void m_setup_debug_messenger ();
    VkResult m_create_debug_messenger (vk::Instance instance,
					const VkDebugUtilsMessengerCreateInfoEXT* create_info,
					const VkAllocationCallbacks* allocator,
					VkDebugUtilsMessengerEXT* debug_messenger);
    void m_destroy_debug_messenger (vk::Instance instance,
				    VkDebugUtilsMessengerEXT debug_messenger,
				    const VkAllocationCallbacks* allocator);
    void m_populate_debug_messenger_create_info (VkDebugUtilsMessengerCreateInfoEXT& create_info);
#endif //NDEBUG
    
    void m_init_window ();
    void m_init_vulkan ();
    void m_main_loop ();

    void m_create_instance ();
    std::vector<const char*> m_get_required_instance_exts ();
    void m_pick_physical_device ();
    queue_family_indices m_find_queue_families (vk::PhysicalDevice physical_device);
    void m_create_logical_device ();
    void m_create_surface ();
    swap_chain_support_details m_query_swap_chain_support (vk::PhysicalDevice physical_device);
    vk::SurfaceFormatKHR m_choose_swap_surface_format (const std::vector<vk::SurfaceFormatKHR>&
						      available_formats);
    vk::PresentModeKHR m_choose_swap_present_mode (const std::vector<vk::PresentModeKHR>&
						  available_present_modes);
    vk::Extent2D m_choose_swap_extent (const vk::SurfaceCapabilitiesKHR& capabilities);
    void m_create_swap_chain ();
    void m_create_image_views ();
    void m_create_graphics_pipeline ();
    static std::vector<char> m_read_file (const std::string& filename);
    vk::ShaderModule m_create_shader_module (const std::vector<char>& code);
    void m_create_render_pass ();
    void m_create_framebuffers ();
    void m_create_command_pool ();
    void m_create_command_buffers ();
    void m_draw_frame ();
    void m_create_sync_objects ();
    void m_recreate_swap_chain ();
    void m_cleanup_swap_chain ();
    static void m_framebuffer_resize_callback (GLFWwindow* window, int width, int height);
    void m_create_vertex_buffer ();
    uint32_t m_find_memory_type (uint32_t type_filter, vk::MemoryPropertyFlags properties);
    void m_create_buffer (vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties,
			   vk::Buffer& buffer, vk::DeviceMemory& buffer_memory);
    void m_copy_buffer (vk::Buffer src_buffer, vk::Buffer dst_buffer, vk::DeviceSize size);
    void m_create_index_buffer ();
    void m_create_descriptor_set_layout ();
    void m_create_uniform_buffers ();
    void m_update_uniform_buffer (uint32_t current_image);
    void m_create_descriptor_pool ();
    void m_create_descriptor_sets ();
    void m_create_texture_image ();
    void m_create_image (uint32_t width, uint32_t height, uint32_t mip_levels,
			  vk::SampleCountFlagBits num_samples, vk::Format format, vk::ImageTiling tiling,
			  vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties, vk::Image& image,
			  vk::DeviceMemory& image_memory);
    vk::CommandBuffer m_begin_single_time_commands ();
    void m_end_single_time_commands (vk::CommandBuffer command_buffer);
    void m_copy_buffer_to_image (vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height);
    void m_transition_image_layout (vk::Image image, vk::Format format, vk::ImageLayout old_layout,
				     vk::ImageLayout new_layout, uint32_t mip_levels);
    void m_create_texture_image_view ();
    vk::ImageView  m_create_image_view (vk::Image image, vk::Format format, vk::ImageAspectFlags aspect_flags,
				       uint32_t mip_levels);
    void m_create_texture_sampler ();
    void m_create_depth_resources ();
    vk::Format m_find_supported_format (const std::vector<vk::Format>& candidates, vk::ImageTiling tiling,
				       vk::FormatFeatureFlags features);
    vk::Format m_find_depth_format ();
    bool m_has_stencil_component (vk::Format format);
    void m_load_model ();
    void m_generate_mipmaps (vk::Image image, vk::Format image_format, int32_t tex_width, int32_t tex_height,
			      uint32_t mip_levels);
    vk::SampleCountFlagBits m_get_max_usable_smaple_count ();
    void m_create_color_resources ();
    void m_get_key_input (GLFWwindow* window);
    void m_get_cursor_input (GLFWwindow* window);
    void m_bind_model (tinyobj::attrib_t* attrib, std::vector<tinyobj::shape_t>* shapes);
  };
} // namespace app

// Templates:
template<> struct
std::hash<app::vulkan::vertex>
{
  size_t operator() (const app::vulkan::vertex& hash_vertex) const
  {
    return ((hash<glm::vec3> () (hash_vertex.pos) ^
	     (hash<glm::vec3> () (hash_vertex.color) << 1)) >> 1) ^
      (hash<glm::vec2> () (hash_vertex.tex_coord) << 1);
  }
};
#endif // APP_VULKAN_HPP
